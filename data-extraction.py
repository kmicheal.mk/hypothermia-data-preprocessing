import os
import vitaldb
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()


#Generates the file name for file lookup.
def get_vital_file_name(count):
    file_name = '000' + str(count)
    if count > 9 and count < 100:
        file_name = '00' + str(count)
    elif count > 99 and count < 1000:
        file_name = '0' + str(count)
    elif count > 999:
        file_name = str(count)

    #downloaded files should be stored in the vital_files directory in this repo.
    return 'vital_files/' + file_name + '.vital'


track_names = [
    'Solar8000/BT',  # Body Temparature
    'Solar8000/ART_DBP',  # Diastolic arterial pressure
    'Solar8000/ART_SBP',  # Systolic arterial pressure
    'Solar8000/HR',  # Heart Rate
]

path = os.path.dirname(os.path.realpath(__file__))

#adjust this to your starting value.
start = 1

#adjust this to the number of records you would want to extract.
#This number is used relative to the start value.
number_of_records = 3000
case_data = pd.read_csv('clinical_data.csv').loc[0:start + number_of_records - 1]

extracted_values = {
    'case_id':[],
    'age': [],
    'sex': [],
    'bmi': [],
    'ane_type': [],
    'emop': [],
    'heart_rate': [],
    'preop_dm': [],
    'preop_htn': [],
    'preop_hb': [],
    'preop_plt': [],
    'preop_gluc': [],
    'op_start_heart_rate': [],
    'op_start_temperature': [],
    'op_start_diastolic': [],
    'op_start_systolic': [],
    'hypothermia_temp': [],
    'anathetic_duration': [],
    'systolic_p_at_hypothermia': [],
    'diastolic_p_at_hypothermia': [],
    'time_to_hypothermia': [],
    'lowest_temperature': [],
    'time_at_lowest_temp': []
}
for index in range(start, start + number_of_records):
    print('-------------' + str(index) + '-----------------')
    # Read .vital file
    case_record = case_data.loc[index - 1]
    file_name = get_vital_file_name(index)
    vf = vitaldb.VitalFile(file_name, track_names)

    # Convert file to dataframe.
    vitals_data = vf.to_pandas(track_names, 60)

    below_31 = vitals_data[vitals_data['Solar8000/BT'] < 31].index.tolist()
    below_31_indices = [a for a in below_31 if a >= int(case_record.opstart/60)]
    vitals_below_31 = vitals_data.loc[below_31_indices].dropna()
    temperature = vitals_below_31['Solar8000/BT']

    # Get lowest temperature
    if(len(temperature) > 0):
        extracted_values['case_id'].append(case_record.caseid)
        extracted_values['sex'].append(case_record.sex)
        extracted_values['age'].append(case_record.age)
        extracted_values['bmi'].append(case_record.bmi)
        extracted_values['emop'].append(case_record.emop)
        extracted_values['ane_type'].append(case_record.ane_type)
        extracted_values['preop_dm'].append(case_record.preop_dm)
        extracted_values['preop_htn'].append(case_record.preop_htn)
        extracted_values['preop_hb'].append(case_record.preop_hb)
        extracted_values['preop_plt'].append(case_record.preop_plt)
        extracted_values['preop_gluc'].append(case_record.preop_gluc)

        hypothermia_temp = max(temperature)
        extracted_values['hypothermia_temp'].append(hypothermia_temp)

        time_at_hypothermia = vitals_below_31[vitals_below_31['Solar8000/BT'] == hypothermia_temp].index.tolist()[0]
        extracted_values['time_to_hypothermia'].append(time_at_hypothermia)
        extracted_values['heart_rate'].append(vitals_below_31.loc[time_at_hypothermia]['Solar8000/HR'])
        extracted_values['diastolic_p_at_hypothermia'].append(vitals_below_31.loc[time_at_hypothermia]['Solar8000/ART_DBP'])
        extracted_values['systolic_p_at_hypothermia'].append(vitals_below_31.loc[time_at_hypothermia]['Solar8000/ART_SBP'])

        #Lowest temp
        lowest_temp = min(temperature)
        extracted_values['lowest_temperature'].append(lowest_temp)

        time_at_lowest_temp = vitals_below_31[vitals_below_31['Solar8000/BT'] == lowest_temp].index.tolist()[0]
        extracted_values['time_at_lowest_temp'].append(time_at_lowest_temp)

        #Get start vitals
        preop_indices = [index for index in vitals_data.index.tolist() if index <= int(case_record.opstart/60)]
        op_start_vitals = vitals_data.loc[max(preop_indices)]
        extracted_values['op_start_temperature'].append(op_start_vitals['Solar8000/BT'])
        extracted_values['op_start_diastolic'].append(op_start_vitals['Solar8000/ART_DBP'])
        extracted_values['op_start_systolic'].append(op_start_vitals['Solar8000/ART_SBP'])
        extracted_values['op_start_heart_rate'].append(op_start_vitals['Solar8000/HR'])

        #Get anathetic duration
        extracted_values['anathetic_duration'].append(int(time_at_hypothermia - (case_record.anestart/60)))


#save extracted data as csv.
data = pd.DataFrame(extracted_values)
data.to_csv('extracted_data.csv', index=False)
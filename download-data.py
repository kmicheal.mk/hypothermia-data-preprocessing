import wget

for x in range(1900, 2001):
    file = str(x) + '.vital'
    url = 'https://physionet.org/files/vitaldb/1.0.0/vital_files/' + file
    print("\n-------------------------" + file + "\n")
    response = wget.download(url, "vital_files/" + file)
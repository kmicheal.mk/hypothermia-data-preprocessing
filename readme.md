# Data Download

Use `pip` to intall pandas and vitaldb python packages, see link below for vitaldb documentation, you can use a virtual enviroment.
https://vitaldb.net/docs/?documentId=1j702xfDef7RNhfZANQQt7tkzzbin2FVe9GAnleDSyzI#h.ydaknx6mraju


run `python data-download.py` to download the files. The current script will download data from caseid 1900 to 2000.
Adjsut these ranges in `data-download.py` and run the script on a different terminal to initiate multiple downloads.
Ensure you have a folder named `vital_files` in the directory where the terminal is opened.


# Data extraction

Run `python data-extraction` to extract data from the downloaded files. The extracted data will be stored in a new csv file named `extracted_data.csv`.